#ifndef BUTTON_CLASS_H
#define BUTTON_CLASS_H

#include <unistd.h>
#include <iostream>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>

#include "gpio.hpp"

class Button {
private:
	GPIO* btn_gpio;

	std::string type;

	int previousState = 0;
	int actualState = 0;

	bool update();
	bool isFallingFront();
	bool isRisingFront();

public:
	Button(int pin);
	Button(int pin, std::string type);
	~Button();

	bool isClicked();

};

#endif
