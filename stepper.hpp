#ifndef STEPPER_CLASS_H
#define STEPPER_CLASS_H

#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <unistd.h>

#include "gpio.hpp"

class Stepper{
private:
	GPIO* pins[4];
	const int motorSpeed = 2400;
	const int countsperrev = 512;
	const int motorsSequence[8][4] = { {1, 0, 0, 0},
													{1, 1, 0, 0},
													{0, 1, 0, 0},
													{0, 1, 1, 0},
													{0, 0, 1, 0},
													{0, 0, 1, 1},
													{0, 0, 0, 1},
													{1, 0, 0, 1}};
	const int motorRelease[4] = {0, 0, 0, 0};

	void clockwise();
	void setOutput(int out);

public:
	Stepper(int* pins, int numPins);
	~Stepper();

	void releaseStepper();
	void rotateDegree(int degree, bool steady);
	void rotateCount(int count, bool steady);

	int get_countsperrev() {return this->countsperrev;};

};

#endif
