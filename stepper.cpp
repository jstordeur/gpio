#include "stepper.hpp"

float map(float value, float fromLowBound, float fromUpBound, float toLowBound, float toUpBound);

using namespace std;

Stepper::Stepper(int* pins_, int numPins) {
	for(int i = 0; i < numPins; i++) {
		this->pins[i] = new GPIO(pins_[i], "out", 0);
	}

}

Stepper::~Stepper() {
	for(int i = 0; i < 4; i++) {
		delete this->pins[i];
	}
}

void Stepper::rotateDegree(int degree, bool steady = true) {
	int bound = map(degree, 0, 360, 0, this->countsperrev);

	for(int count = 0; count < bound; count++) {
		this->clockwise();
	}
	if(!steady) {
		this->releaseStepper();
	}
}

void Stepper::rotateCount(int count, bool steady) {
	for(int i = 0; i < count; i++) {
		this->clockwise();
	}
	if(!steady) {
		this->releaseStepper();
	}
}

void Stepper::clockwise() {
	for(int i = 0; i < 8; i++) {
		this->setOutput(i);
		usleep(this->motorSpeed);
	}
}

void Stepper::setOutput(int out) {
	for(int i = 0; i < 4; i++) {
		this->pins[i]->setstate(this->motorsSequence[out][i]);
	}
}

void Stepper::releaseStepper() {
	for(int i = 0; i < 4; i++) {
		this->pins[i]->setstate(this->motorRelease[i]);
	}
}

float map(float value, float fromLowBound, float fromUpBound, float toLowBound, float toUpBound) {
	return value / (fromUpBound - fromLowBound) * (toUpBound - toLowBound);
}
