#include "gpio.hpp"

using namespace std;

GPIO::GPIO(int pinNumber, std::string dir, int state=LOW) {
	this->pinNumber = pinNumber;

	if(dir == "out" || dir == "in") {
		this->dir = dir;
	}
	else
		this-> dir = "out";

	this->state = state;

	this->export_gpio();
	this->setdir(this->dir);
	this->setstate(this->state);
}

GPIO::~GPIO(){
	this->unexport_gpio();
}

bool GPIO::export_gpio() {
	string export_str = "/sys/class/gpio/export";
	ofstream exportstream(export_str.c_str());

	if (!exportstream){
		cout << " OPERATION FAILED: Unable to export GPIO" <<
			this->pinNumber <<
			" ." << endl;
		return false;
	}

	exportstream << this->pinNumber ;
	exportstream.close();

	return true;
}

bool GPIO::unexport_gpio() {
	string unexport_str = "/sys/class/gpio/unexport";
	ofstream unexportstream(unexport_str.c_str());

	if (!unexportstream){
		cout << " OPERATION FAILED: Unable to unexport GPIO" <<
			this->pinNumber << " ." << endl;
		return false;
	}

	unexportstream << this->pinNumber ;
	unexportstream.close();

	return true;
}

bool GPIO::setdir(std::string dir) {
	string setdir_str ="/sys/class/gpio/gpio" + to_string(this->pinNumber) + "/direction";
	ofstream setdirstream(setdir_str.c_str());

	if (!setdirstream){
		cout << " OPERATION FAILED: Unable to set direction of GPIO" <<
			this->pinNumber << " ." << endl;
		return false;
	}

	setdirstream << dir;
	setdirstream.close();

	return true;
}

bool GPIO::setstate(int state) {

	string setval_str = "/sys/class/gpio/gpio" + to_string(this->pinNumber) + "/value";
	ofstream setvalstream(setval_str.c_str());

	if (!setvalstream){
		cout << " OPERATION FAILED: Unable to set the value of GPIO" <<
			this->pinNumber << " ." << endl;
		return false;
	}

	setvalstream << state ;//write value to value file
	setvalstream.close();// close value file
	return 0;

	return true;
}

bool GPIO::getstate(int& state) {
	string buf;

	string getval_str = "/sys/class/gpio/gpio" + to_string(this->pinNumber) + "/value";
	ifstream getvalstream(getval_str.c_str());

	if (!getvalstream){
		cout << " OPERATION FAILED: Unable to get value of GPIO" <<
			this->pinNumber << " ." << endl;
		return false;
	}

	getvalstream >> buf ;  //read gpio value

	this->state = stoi(buf);

	state = this->state;

	getvalstream.close(); //close the value file

	return true;
}

bool GPIO::toogle() {
	if(this->state){
		this->state = 0;
		this->setstate(0);
	}
	else {
		this->state = 1;
		this->setstate(1);
	}

	return true;
}
