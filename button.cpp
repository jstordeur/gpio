#include "button.hpp"

using namespace std;

Button::Button(int pin) {
	this->btn_gpio = new GPIO(pin, "in", 0);

	this->type = "pullup";

	this->update(); this->update();
}

Button::Button(int pin, string type) : Button(pin) {
	this->type = type;
}

Button::~Button() {
	delete btn_gpio;
}

bool Button::update() {
	this->previousState = this->actualState;
	btn_gpio->getstate(this->actualState);

	return true;
}

bool Button::isFallingFront() {
	return previousState == 1 && actualState == 0;
}

bool Button::isRisingFront() {
	return previousState == 0 && actualState == 1;
}

bool Button::isClicked() {
	this->update();

	usleep(20000);

	if(this->type == "pullup")
		return this->isRisingFront();
	else if(this->type == "pulldown")
		return this->isFallingFront();
	else {
		std::cout << "WTF ?\n";
		return false;
	}
}
