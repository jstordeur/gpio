#ifndef GPIO_CLASS_H
#define GPIO_CLASS_H

#include <string>
#include <fstream>
#include <iostream>
#include <sstream>

#define LOW 0
#define HIGH 1

class GPIO {
private:
	int pinNumber;
	std::string dir;
	int state;

	bool export_gpio();
	bool unexport_gpio();

public:
	GPIO(int pinNumber, std::string dir, int state);
	~GPIO();

	bool setdir(std::string dir);
	bool setstate(int state);
	bool getstate(int& state);
	bool toogle();

	int get_pinNumber() {return this->pinNumber;};
	std::string get_dir() {return this->dir;};
};

#endif
